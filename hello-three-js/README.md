# Hello ThreeJS

Same example as [hello-three-vanilla](../hello-three-vanilla/README.md), but using yarn and parcel.

```
yarn build
yarn dev
```
