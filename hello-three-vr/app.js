import * as THREE from 'three'
import { WEBVR } from './src/WebVR'

let scene, camera, renderer
const vrMode = true

scene = new THREE.Scene()
camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000)
renderer = new THREE.WebGLRenderer()
renderer.setSize(window.innerWidth, window.innerHeight)
document.body.appendChild(renderer.domElement)
renderer.setClearColor(0x222233)
if(vrMode) {
    renderer.vr.enabled = true
    document.body.appendChild(WEBVR.createButton(renderer))
}
else { camera.position.z = 5 }

initScene(scene)
renderer.setAnimationLoop(animate)

function animate() {
    renderer.render(scene, camera)
}

function initScene(scene) {
    let boxGeo = new THREE.BoxGeometry(1, 1, 1)
    let mat01 = new THREE.MeshBasicMaterial({color: 0xff22ff})
    let mat02 = new THREE.MeshBasicMaterial({color: 0x22ffff})
    let mat03 = new THREE.MeshBasicMaterial({color: 0xffff22})

    let cube01 = new THREE.Mesh(boxGeo, mat01)
    let cube02 = new THREE.Mesh(boxGeo, mat02)
    let cube03 = new THREE.Mesh(boxGeo, mat03)

    let cubes = [ cube01, cube02, cube03 ]
    for(const c of cubes) {
        scene.add(c)
    }

    cube01.position.x = -3
    cube02.position.x = 3
    cube03.position.z = -2
}
