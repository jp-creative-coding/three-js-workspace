import * as THREE from 'three'
import { OBJLoader } from 'three/examples/jsm/loaders/OBJLoader'
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls'
import randomInt from 'random-int'

let scene, camera, renderer, gltfLoader, objLoader, controls
let matPink, matBlue, matYellow
let stars = []
let starRotationsSpeeds = []

init()

function init() {
    scene = new THREE.Scene()
    window.scene = scene
    camera = new THREE.PerspectiveCamera(75, window.innerWidth/window.innerHeight, 0.1, 1000)
    renderer = new THREE.WebGLRenderer({antialias: true})
    renderer.setSize(window.innerWidth, window.innerHeight)
    renderer.setClearColor(0x525283)
    document.body.appendChild(renderer.domElement)
    camera.position.y = 10
    camera.position.z = 6
    camera.position.x = 12

    controls = new OrbitControls(camera, renderer.domElement)

    let ambLight = new THREE.AmbientLight(0xeeeeee)
    scene.add(ambLight)
    let dirLight = new THREE.DirectionalLight(0xffffff, 0.5)
    scene.add(dirLight)

    initMaterials()

    gltfLoader = new GLTFLoader()
    loadGltf('../assets/Unicorn_01.gltf', placeUnicorn, 'unicorn')
    loadGltf('../assets/Rainbow_01.gltf', placeRainbow, 'rainbow')

    objLoader = new OBJLoader()
    loadObj('../assets/PUSHILIN_star.obj', placeStars, 'BaseStar')


    window.addEventListener('resize', onResize, false)

    update()
}

function loadObj(path, onDone = () => { return void 0 }, name, material) {
    objLoader.load(
        path,
        function(object) {
            scene.add(object)
            if(name) object.name = name 
            if(material) {
                object.traverse(function(child){
                    if(child instanceof THREE.Mesh) {
                        child.material = material
                    }
                })
            }
            console.log(object)
            return onDone(object)
        },
        function(xhr) {
            console.log((xhr.loaded / xhr.total * 100) + '% loaded')
        },
        function(error) {
            console.log('An error happened', error)
        }
    )
}

function loadGltf(path, onDone, name = 'scene') {
    gltfLoader.load(    
        path,
        function(gltf) {
            console.log(gltf)
            gltf.scene.name = name
            scene.add(gltf.scene)
            console.log
            onDone()
        },
        function(xhr) {
            console.log((xhr.loaded / xhr.total * 100) + '% loaded')
        },
        function(error) {
            console.log('An error happened', error)
        }
    )
}

/**
* App loop
*/

function update() {
    requestAnimationFrame(update)
    controls.update()
    animateStars()
    renderer.render(scene, camera)
}

function onResize() {
    camera.aspect = window.innerWidth / window.innerHeight
    camera.updateProjectionMatrix()
    renderer.setSize(window.innerWidth, window.innerHeight)
}

/**
* Handle assets
*/

function initMaterials() {
    matPink = new THREE.MeshLambertMaterial({ color: 0xffaaff })
    matBlue = new THREE.MeshLambertMaterial({ color: 0xaaaaff })
    matYellow = new THREE.MeshLambertMaterial({ color: 0xffffaa })
}

function placeUnicorn() {
    let unicorn = scene.getObjectByName('unicorn')
    console.log('Found the unicorn scene! ', unicorn)
    unicorn.position.y = -10
    unicorn.scale.x = 0.5
    unicorn.scale.set(0.8, 0.8, 0.8)
}

function placeRainbow() {
    let rainbow = scene.getObjectByName('rainbow')
    console.log('Found the rainbow scene!', rainbow)
    rainbow.rotation.set(0, 90, 0)
    rainbow.position.set(0, -10, -10)
    rainbow.scale.set(3, 3, 3)
}

const starConfig = {
    count: 40,
    bounds: {
        x: {
            min: -30,
            max: 30
        },
        y: {
            min: -10,
            max: 25
        },
        z: {
            min: -30,
            max: 30
        }
    },
    scale: {
        min: 0.5,
        max: 2
    },
    rotation: {
        min: 0.1,
        max: 5
    },
    animate: {
        min: 0.005,
        max: 0.0075
    }
}

function placeStars(baseStar) {
    const cnf = starConfig
    let matIndex = 0
    const materials = [ matPink, matYellow, matBlue ]
    for (let i = 0; i < cnf.count; i++) {
        let star = baseStar.clone()
        star.name = 'star' + i
        let x = randomInt(cnf.bounds.x.min, cnf.bounds.x.max)
        let y = randomInt(cnf.bounds.y.min, cnf.bounds.y.max)
        let z = randomInt(cnf.bounds.z.min, cnf.bounds.z.max)
        star.position.set(x, y, z)
        let s = randomInt(cnf.scale.min, cnf.scale.max)
        star.scale.set(s, s, s)
        star.rotation.y = randomInt(cnf.rotation.min, cnf.rotation.max)
        star.rotation.z = randomInt(cnf.rotation.min, cnf.rotation.max)

        star.children[0].material = materials[matIndex]
        matIndex++
        if(matIndex === materials.length) matIndex = 0        

        console.log(star)
        stars.push(star)
        // randomize constant speed for each star
        starRotationsSpeeds.push(randomInt(10, 100) / 10000)
        scene.add(star)
    }
}

function animateStars() {
    for (let i = 0; i < stars.length; i++) {
        const star = stars[i]
        const speed = starRotationsSpeeds[i]
        star.rotation.x += speed
        star.rotation.y += speed
        star.rotation.z += speed
    }
}
